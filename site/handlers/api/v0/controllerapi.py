# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from google.appengine.ext import webapp
from google.appengine.ext import db
from django.utils import simplejson as json
from site.utility.requests import BaseRequestHandler
from site.models.controller import Controller
from site.utility import geo
from datetime import datetime, timedelta
import uuid, sys


class ControllerAPIHandler(BaseRequestHandler):

	def get(self):
		# we will be outputting json
		self.request.headers["Content-Type"] = "application/json"

		query = self.request.get("query")
		if query == "all":
			return self.get_all()
		elif query == "in_range":
			return self.get_in_range()
		else:
			return self.error_response(400, "invalid query")
	
	def get_all(self):
		controllers = Controller.all().filter("expires > ", datetime.now())
		clist = [self.build_controller_dict(c) for c in controllers]
		json.dump(clist, self.response.out)

	def get_in_range(self):
		try:
			lat = float(self.request.get("lat", None))
			lon = float(self.request.get("lon", None))
		except (TypeError, ValueError):
			return self.error_response(400, "insufficient or invalid arguments")

		controllers = Controller.get_active_list()
		clist = []
		for c in controllers:
			d = geo.distance_nm(lat, lon, c.lat, c.lon)
			if d <= c.get_range():
				cdict = self.build_controller_dict(c, dist = d)
				clist.append(cdict)
		json.dump(clist, self.response.out);

	def post(self):
		callsign = self.request.get("callsign", None)
		duty = self.request.get("duty", None)
		freq = self.request.get("freq", None)
		lat = self.request.get("lat", None)
		lon = self.request.get("lon", None)
		murl = self.request.get("murl", None)

		# make sure we got all parameters
		if None in (callsign, duty, freq, lat, lon, murl):
			return self.error_response(400, "insufficient arguments")

		# make sure lat and lon are ok
		try:
			lat = float(lat)
			lon = float(lon)
		except (TypeError, ValueError):
			return self.error_response(400, "invalid lat or lon argument(s)")

		# make sure there is not an active controller with callsign already
		c = Controller.all().filter("callsign = ", callsign).get()
		if c and c.is_active():
			return self.error_response(403, "callsign is already in use")

		c = c or Controller.get_inactive_or_new()

		# try filling it in and see if it validates
		try:
			c.callsign = callsign
			c.duty = duty
			c.frequency = freq
			c.lat = lat
			c.lon = lon
			c.murl = murl
			c.uuid = uuid.uuid4().hex
			c.expires = datetime.now() + timedelta(minutes = 5)
			c.put()
		except db.ValidationError:
			return self.error_response(400, "bad value(s)")

		self.response.set_status(201)
		json.dump({ "uuid": c.uuid }, self.response.out)

	def put(self):
		uuid = self.request.get("uuid")
		if not uuid:
			return self.error_response(400, "no uuid")

		c = Controller.all().filter("uuid = ", uuid).get()
		if not c:
			return self.error_response(400, "invalid request")

		if c.expires < datetime.now():
			return self.error_response(400, "expired")

		c.expires = datetime.now() + timedelta(minutes = 5)
		c.put()

	def delete(self):
		uuid = self.request.get("uuid")
		if not uuid:
			return self.error_response(400, "no uuid")

		c = Controller.all().filter("uuid = ", uuid).get()
		if not c or not c.is_active():
			return self.error_response(400, "invalid request")

		c.expires = datetime.now()
		c.put()

	def build_controller_dict(self, controller, **extra):
		data = {
			"callsign": controller.callsign,
			"freq": controller.frequency,
			"lat": controller.lat,
			"lon": controller.lon,
			"murl": controller.murl,
			"duty": controller.duty,
			"radius": controller.get_range()
		}
		data.update(extra)
		return data

	def error_response(self, code, msg):
		json_data = {
			"message": msg
		}
		self.error(code)
		json.dump(json_data, self.response.out)
