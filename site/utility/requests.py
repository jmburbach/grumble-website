# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from google.appengine.ext import webapp


class BaseRequestHandler(webapp.RequestHandler):

	def initialize(self, request, response):
		webapp.RequestHandler.initialize(self, request, response)
		if request.path.endswith("/") and not request.path == "/":
			redirect = request.path[:-1]
			if request.query_string:
				redirect += "?" + request.query_string
			return self.redirect(redirect, permanent = True)
