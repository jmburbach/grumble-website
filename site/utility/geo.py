# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import math

MEAN_EARTH_RADIUS_KM = 6371.0
KM_TO_NM = 0.539956803


def distance(lat1, lon1, lat2, lon2):
	""" Return the distance, in kilometers, between two points. """
	lat1 = math.radians(lat1)
	lon1 = math.radians(lon1)
	lat2 = math.radians(lat2)
	lon2 = math.radians(lon2)
	dlon = lon2 - lon1
	dlat = lat2 - lat1
	a = ( math.sin(dlat * 0.5) * math.sin(dlat * 0.5) +
			math.cos(lat1) * math.cos(lat2) *
			math.sin(dlon * 0.5) * math.sin(dlon * 0.5) )
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
	return MEAN_EARTH_RADIUS_KM * c 

def distance_nm(lat1, lon1, lat2, lon2):
	return distance(lat1, lon1, lat2, lon2) * KM_TO_NM

