# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from google.appengine.ext import db
from datetime import datetime
import re


DUTY_RE = re.compile(r"(DEL|GND|TWR|DEP|APP|CTR)")
FREQ_RE = re.compile(r"^1(18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35)\.[0-9](00|25|50|75)$")

DUTY_RANGE = {
	"DEL": 10.0,
	"GND": 10.0,
	"TWR": 30.0,
	"DEP": 100.0,
	"APP": 100.0,
	"CTR": 400.0
}

def duty_validator(duty):
	if not DUTY_RE.match(duty):
		raise db.ValidationError("invalid atc duty")

def frequency_validator(freq):
	freq = freq.strip()
	if not FREQ_RE.match(freq):
		raise db.ValidationError("invalid frequency")

def latitude_validator(lat):
	if abs(lat) > 90.0:
		raise db.ValidationError("invalid latitude")

def longitude_validator(lon):
	if abs(lon) > 180.0:
		raise db.ValidationError("invalid longitude")


class Controller(db.Model):
	callsign = db.StringProperty()
	duty = db.StringProperty(validator = duty_validator, default = "GND")
	frequency = db.StringProperty(validator = frequency_validator, default = "118.000")
	lat = db.FloatProperty(validator = latitude_validator, default = 0.0)
	lon = db.FloatProperty(validator = longitude_validator, default = 0.0)
	murl = db.StringProperty()
	uuid = db.StringProperty()
	expires = db.DateTimeProperty()

	def get_range(self):
		return DUTY_RANGE[self.duty]

	def is_active(self):
		return self.expires > datetime.now()

	@staticmethod
	def get_active_list():
		return Controller.all().filter("expires > ", datetime.now())

	@staticmethod
	def get_inactive_or_new():
		c = Controller.all().filter("expires < ", datetime.now()).get()
		return c or Controller()
