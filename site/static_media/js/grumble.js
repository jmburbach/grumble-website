/* namespace */
var grumble = {};

grumble.API_VERSION = 0;

grumble.DEG_TO_RAD = (Math.PI / 180.0);
grumble.RAD_TO_DEG = (180.0 / Math.PI);
grumble.NM_TO_RAD = (Math.PI / (180.0 * 60.0));
grumble.RAD_TO_NM = ((180.0 * 60.0) / Math.PI);



/** grumble.GeoCoord {{{
 *
 * Represents a geographical coordinate (lat/lon) and contains usefule operations
 * for working with and doing calculations on them.
 */
grumble.GeoCoord = function(lat, lon)
{
	// private
	var m_lat = lat;
	var m_lon = lon;
	
	/** get latitude */
	this.lat = function() { return m_lat; }

	/** get longitude */
	this.lon = function() { return m_lon; }

	/** 
	 * Given an initial true course in degrees and a distance in nautical miles, will
	 * calculate and return a new GeoCoord object representating that point.
	 */
	this.get_destination = function(true_course_deg, distance_nm)
	{
		var R = true_course_deg * grumble.DEG_TO_RAD;
		var D = distance_nm * grumble.NM_TO_RAD;
		var lat1 = m_lat * grumble.DEG_TO_RAD;
		var lon1 = m_lon * grumble.DEG_TO_RAD;
		var lat2, lon2;					

		lat2 = Math.asin(Math.sin(lat1) * Math.cos(D) + Math.cos(lat1) * Math.sin(D) * Math.cos(R));

		if (Math.cos(lat2) == 0) {
			lon2 = lon1;
		}
		else {
			lon2 = ((lon1 - Math.asin(Math.sin(R) * Math.sin(D) / Math.cos(lat2)) + Math.PI) % (2 * Math.PI)) - Math.PI;
		}

		return new grumble.GeoCoord(lat2 * grumble.RAD_TO_DEG, lon2 * grumble.RAD_TO_DEG);
	}

	/**
	 * Convenience method to convert ourself to a CM.LatLng object.
	 */
	this.to_cloudmade = function()
	{
		return new CM.LatLng(m_lat, m_lon);
	}
}

/** 
 * Static convenience method to create a grumble.GeoCoord from a CM.LatLng object.
 */
grumble.GeoCoord.from_cloudmade = function(latlng)
{
	return new grumble.GeoCoord(latlng.lat(), latlng.lng());
}
/* end grumble.GeoCoord }}}*/

grumble.ControllerItem = function(controller)
{
	this.controller = controller;
	this.pos = new CM.LatLng(this.controller.lat, this.controller.lon);
	this.latlngs = new Array();
	this.overlays = new Array();

	var color, stroke = 2, marker_pos = this.pos;
	var icon = new CM.Icon();
	icon.iconSize = new CM.Size(34, 16);

	if (this.controller.duty == "DEL") {
		color = "#FF9911";
		icon.image = "/media/images/atc_del.png";
		icon.iconAnchor = new CM.Point(56, 8);
	}
	else if (this.controller.duty == "GND") {
		color = "#FF0000";
		icon.image = "/media/images/atc_gnd.png";
		icon.iconAnchor = new CM.Point(17, 8);
	}
	else if (this.controller.duty == "TWR") {
		color = "#00FF00";
		icon.image = "/media/images/atc_twr.png";
		icon.iconAnchor = new CM.Point(-22, 8);
	}
	else if (this.controller.duty == "DEP") {
		color = "#0000FF";
		icon.image = "/media/images/atc_dep.png";
		marker_pos = grumble.GeoCoord.from_cloudmade(this.pos).get_destination(90.0, this.controller.radius).to_cloudmade();
	}
	else if (this.controller.duty == "APP") {
		color = "#FFFF00";
		icon.image = "/media/images/atc_app.png";
		marker_pos = grumble.GeoCoord.from_cloudmade(this.pos).get_destination(270.0, this.controller.radius).to_cloudmade();
		stroke = 6;
	}
	else {
		color = "#00FFFF";
		icon.image = "/media/images/atc_ctr.png";
		marker_pos = grumble.GeoCoord.from_cloudmade(this.pos).get_destination(0, this.controller.radius).to_cloudmade();
	}

	var center = grumble.GeoCoord.from_cloudmade(this.pos);
	for (var i = 0; i < 361; ++i) {
		var pt = center.get_destination(i, this.controller.radius).to_cloudmade();
		this.latlngs.push(pt);
	}
	
	this.overlays.push(new CM.Marker(marker_pos, {icon: icon, title: this.controller.callsign + " on " + this.controller.frequency}));
	this.overlays.push(new CM.Polyline(this.latlngs, color, stroke));
	
	this.add_overlays = function(map)
	{
		for (var i = 0; i < this.overlays.length; ++i) {
			map.addOverlay(this.overlays[i]);
		}
	}
	
	this.remove_overlays = function(map)
	{
		for (i in this.overlays) {
			map.removeOverlay(this.overlays[i]);
		}
		delete this.overlays;
		this.overlays = new Array();
	}
}
/*
grumble.ControllerItem = function(controller)
{
	this.controller = controller;
	this.geocoord = new grumble.GeoCoord(this.controller.lat, this.controller.lon);
	this.overlays = new Array();

	switch (this.controller.duty) {
		case "DEL":
			this.color = "#FF9911";
			break;
		case "GND":
			this.color = "#FF0000";
			break;
		case "TWR":
			this.color = "#00FF00";
			break;
		case "DEP":
			this.color = "#0000FF";
			break;
		case "APP":
			this.color = "#FFFF00";
			break;
		case "CTR":
			this.color = "#00FFFF";
			break;
		default:
			this.color = "FFFFFF";
			break;
	}

	this.add_overlays = function(map)
	{
		switch (this.controller.duty) {
			case "DEL":
			case "DEP":
				this.create_dashed_lines(3, 2, 6);
				break;
			default:
				this.create_dashed_lines(0, 2, 6);
				break;
		}

		for (var i = 0; i < this.overlays.length; ++i) {
			map.addOverlay(this.overlays[i]);
		}
	}
	
	this.remove_overlays = function(map)
	{
		for (o in this.overlays) {
			map.removeOverlay(o);
		}
		delete this.overlays;
		this.overlays = new Array();
	}

	this.create_dashed_lines = function(start, step, stride)
	{
		for (var i = start; i < 360 + start; i += stride) {
			var polyline = new CM.Polyline([
				this.geocoord.get_destination(i, this.controller.radius).to_cloudmade(),
				this.geocoord.get_destination(i + step, this.controller.radius).to_cloudmade()
			], this.color, 2);
			this.overlays.push(polyline);
		}
	}
}
*/

/** grumble.Map {{{
 *
 */
grumble.Map = function(div_id)
{
	this.cloudmade = new CM.Tiles.CloudMade.Web({ key: "BC9A493B41014CAABB98F0471D759707", styleId: "19378" });
	this.map = new CM.Map(div_id, this.cloudmade);
	this.map.setCenter(new CM.LatLng(41.514, -15.137), 3);
	
	this.map.addControl(new CM.LargeMapControl());
	this.map.addControl(new CM.ScaleControl());


	this.controller_map = new Object(); // { callsign: { controller: {}, overlay: CM.Overlay } }
	this.perform_controller_request();
}

grumble.Map.prototype.remove_controller_item = function(callsign)
{
	this.controller_map[callsign].remove_overlays(this.map);
	delete this.controller_map[callsign];
}

grumble.Map.prototype.add_controller_item = function(item)
{
	this.controller_map[item.controller.callsign] = item;
	item.add_overlays(this.map);
}

grumble.Map.prototype.handle_controller_response = function(response)
{
	var rmap = new Object();
	for (var i = 0; i < response.length; ++i) {
		var ctlr = response[i];
		rmap[ctlr.callsign] = ctlr;
	}

	for (var key in this.controller_map) {
		if (rmap[key] == undefined) {
			this.remove_controller_item(key);
		}
	}

	for (var key in rmap) {
		if (this.controller_map[key] == undefined) {
			this.add_controller_item(new grumble.ControllerItem(rmap[key]));
		}
	}
}

grumble.Map.prototype.perform_controller_request = function()
{
	var obj = this;
	$.getJSON("/api/v"+grumble.API_VERSION+"/controllers?query=all", function(data) {
		obj.handle_controller_response(data);
	});
}

/* end grumble.Map }}}*/
