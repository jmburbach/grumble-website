# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
try:
	import json
except:
	import simplejson as json

import httplib, urllib, socket
from operator import attrgetter


HTTP_HEADERS = { 
	"Content-Type": "application/x-www-form-urlencoded",
	"Accept": "application/json"
}

DEFAULT_HOST = "grumble-fg.appspot.com"
DEFAULT_PORT = 80


class Controller:
	callsign = ""
	frequency = ""
	duty = "" 
	lat = 0.0
	lon = 0.0
	dist = 0.0
	uuid = ""
	

class GrumbleConnection(object):

	def __init__(self):
		self.headers = {
			"Content-Type": "application/x-www-form-urlencoded",
			"Accept": "application/json"
		}
		self.connection = None

	def connect(self, host = DEFAULT_HOST, port = DEFAULT_PORT, timeout = 15):
		self.connection = httplib.HTTPConnection(host, port, timeout = timeout)
		try:
			self.connection.connect()
		except socket.error, e:
			self.connection = None			
			raise IOError(e[0], e[1])

	def disconnect(self):
		if self.connection:
			self.connection.close()
		self.connection = None

	def query_controllers(self, query, lat = None, lon = None):
		assert self.connection is not None

		if query not in ("all", "in_range"):
			raise ValueError("invalid query name")
		
		if query == "in_range" and not (lat or lon):
			raise ValueError("`in_range' query requires valid lat and lon arguments")

		params = {
			"query": query,
		}

		if lat:
			params["lat"] = lat

		if lon:
			params["lon"] = lon

		self.connection.request("GET", "/api/v0/controllers?" + urllib.urlencode(params), headers = HTTP_HEADERS)
		response = self.connection.getresponse()
		response_data = json.load(response)

		if response.status != 200:
			raise IOError(response.status, response.reason, response_data["message"])

		controllers = []
		for d in response_data:
			c = Controller()
			c.callsign = d["callsign"]
			c.frequency = d["freq"]
			c.lat = float(d["lat"])
			c.lon = float(d["lon"])
			c.duty = d["duty"]
			c.murl = d["murl"]
			c.dist = d.get("dist")
			controllers.append(c)
	
		return sorted(controllers, key = attrgetter("dist"))

	def post_controller(self, controller):
		assert self.connection is not None		
		self.connection.request("POST", "/api/v0/controllers", urllib.urlencode({
				"callsign": controller.callsign,
				"freq": controller.frequency,
				"duty": controller.duty,
				"lat": controller.lat,
				"lon": controller.lon,
				"murl": controller.murl
			}), headers = HTTP_HEADERS)

		response = self.connection.getresponse()
		data = json.load(response)

		if response.status != 201:			
			raise IOError(response.status, response.reason, data["message"])

		controller.uuid = data["uuid"]
		return controller

	def put_controller(self, controller):
		assert self.connection is not None
		self.connection.request("PUT", "/api/v0/controllers?" + urllib.urlencode({
			"callsign": controller.callsign,
			"freq": controller.frequency,
			"duty": controller.duty,
			"lat": controller.lat,
			"lon": controller.lon,
			"murl": controller.murl,
			"uuid": controller.uuid
		}), headers = HTTP_HEADERS)

		response = self.connection.getresponse()

		if response.status != 200:
			data = json.load(response)
			raise IOError(response.status, response.reason, data["message"])

		return controller

	def delete_controller(self, controller):
		assert self.connection is not None
		self.connection.request("DELETE", "/api/v0/controllers?" + urllib.urlencode({
			"callsign": controller.callsign,
			"freq": controller.frequency,
			"duty": controller.duty,
			"lat": controller.lat,
			"lon": controller.lon,
			"murl": controller.murl,
			"uuid": controller.uuid
		}), headers = HTTP_HEADERS)
		
		response = self.connection.getresponse()

		if response.status != 200:
			data = json.load(response)
			raise IOError(response.status, response.reason, data["message"])

