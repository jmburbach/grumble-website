# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from urllib import urlencode
from httplib import HTTPConnection
import unittest, json

class APIv0_Tests(unittest.TestCase):
	POSTED = []

	def setUp(self):
		self.conn = HTTPConnection("localhost", 8080)
		self.conn.connect()
		self.headers = {
			"Content-Type": "application/x-www-form-urlencoded",
			"Accept": "application/json"
		}

	def tearDown(self):
		self.conn.close()
	
	def test_01_post_controller(self):
		params = {
			"callsign": "KIND_TWR",
			"freq": "120.900",
			"lat": 39.7172991,
			"lon": -86.2946612,
			"murl": "mumble://foo.com/KBWI_TWR/?version=1.2.2",
			"duty": "TWR"
		}
		self.conn.request("POST", "/api/v0/controllers", urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEqual(response.status, 201)
		data = json.load(response)
		self.assert_(data.get("uuid"))
		params["uuid"] = data.get("uuid")
		APIv0_Tests.POSTED.append(params)

	def test_02_post_duplicate_controller(self):
		self.conn.request("POST", "/api/v0/controllers", urlencode(APIv0_Tests.POSTED[0]), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 403)
		self.assertEquals(json.load(response)["message"], "callsign is already in use")

	def test_03_post_controller_with_bad_params(self):
		params = {
			"callsign": "KBWI_TWR",
			"freq": "120.900",
			"murl": "mumble://foo.com/KBWI_TWR/?version=1.2.2",
			"duty": "TWR"
		}
		self.conn.request("POST", "/api/v0/controllers", urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 400)
		self.assertEquals(json.load(response)["message"], "insufficient arguments") 
		
		params["lat"] = "abc"
		params["lon"] = "def"
		self.conn.request("POST", "/api/v0/controllers", urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 400)
		self.assertEquals(json.load(response)["message"], "invalid lat or lon argument(s)")
		
		params["lat"] = 91.0
		params["lon"] = -200.0
		self.conn.request("POST", "/api/v0/controllers", urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 400)
		self.assertEquals(json.load(response)["message"], "bad value(s)")

	def test_04_query_all(self):
		self.conn.request("GET", "/api/v0/controllers?" + urlencode({"query": "all"}), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 200)
		data = json.load(response)
		self.assert_(len(data) >= len(APIv0_Tests.POSTED))

	def test_05_query_in_range(self):
		params = {
			"query": "in_range",
			"lat": 39.7172991,
			"lon": -86.2946612,
		}
		self.conn.request("GET", "/api/v0/controllers?" + urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 200)
		data = json.load(response)
		self.assert_(len(data))

	def test_06_query_in_range_with_bad_params(self):
		params = { "query": "in_range" }
		self.conn.request("GET", "/api/v0/controllers?" + urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 400)
		self.assertEquals(json.load(response)["message"], "insufficient or invalid arguments")

		params["lat"] = "a"
		params["lon"] = "ss"
		self.conn.request("GET", "/api/v0/controllers?" + urlencode(params), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 400)
		self.assertEquals(json.load(response)["message"], "insufficient or invalid arguments")

	def test_07_put_controller(self):
		self.conn.request("PUT", "/api/v0/controllers?" + urlencode(APIv0_Tests.POSTED[0]), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 200)

	def test_08_put_controller_with_bad_uuid(self):
		self.conn.request("PUT", "/api/v0/controllers?" + urlencode({}), headers = self.headers)
		response = self.conn.getresponse()
		data = json.load(response)
		self.assertEquals(response.status, 400)
		self.assertEquals(data["message"], "no uuid")
		
		self.conn.request("PUT", "/api/v0/controllers?" + urlencode({"uuid": "ax34dfg"}), headers = self.headers)
		response = self.conn.getresponse()
		data = json.load(response)
		self.assertEquals(response.status, 400)
		self.assertEquals(data["message"], "invalid request")

	def test_09_delete_controller(self):
		for c in APIv0_Tests.POSTED:
			self.conn.request("DELETE", "/api/v0/controllers?" + urlencode(c), headers = self.headers)
			response = self.conn.getresponse()
			self.assertEquals(response.status, 200)
		
		self.conn.request("GET", "/api/v0/controllers?" + urlencode({"query": "all"}), headers = self.headers)
		response = self.conn.getresponse()
		self.assertEquals(response.status, 200)
		data = json.load(response)
		self.assert_(len(data) == 0)

	def test_10_delete_controller_with_bad_uuid(self):
		self.conn.request("DELETE", "/api/v0/controllers?" + urlencode({}), headers = self.headers)
		response = self.conn.getresponse()
		data = json.load(response)
		self.assertEquals(response.status, 400)
		self.assertEquals(data["message"], "no uuid")
		
		self.conn.request("DELETE", "/api/v0/controllers?" + urlencode({"uuid": "ax34dfg"}), headers = self.headers)
		response = self.conn.getresponse()
		data = json.load(response)
		self.assertEquals(response.status, 400)
		self.assertEquals(data["message"], "invalid request")



if __name__ == "__main__":
	suite = unittest.TestLoader().loadTestsFromTestCase(APIv0_Tests)
	unittest.TextTestRunner(verbosity = 2).run(suite)

