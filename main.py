# This file is part of the Grumble project. <http://gitorious.org/grumble>
#
# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under version 3 of the GNU General Public License as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

from site.handlers.frontpage import FrontPageHandler
from site.handlers.controller import ControllerHandler
from site.handlers.api.v0.controllerapi import ControllerAPIHandler


application = webapp.WSGIApplication([
		("/", FrontPageHandler),
		("/controllers/?", ControllerHandler),
		("/api/v0/controllers/?", ControllerAPIHandler)
	],
	debug = True
)


if __name__ == "__main__":
	run_wsgi_app(application)
